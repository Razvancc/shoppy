from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView

def home_page_view(request):
    context = {
        'blog_entries': [
            {
                'title': 'Hello, world!',
                'body': 'I have created my first template in Django!'
            },
            {
                'title': 'A title',
                'body': 'And a description.'
            }

        ]
    }
    return render(request, 'index.html', context)

class AboutView(TemplateView):
    template_name = "about.html"