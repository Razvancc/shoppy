from django.urls import path
from . import views
from pages.views import AboutView

urlpatterns = [
    path('', views.home_page_view, name='home'),
    path('about/', AboutView.as_view())
]