from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
from .models import Product, Category
from django.shortcuts import render


class ProductListView(ListView):
    model = Product
    template_name = "product_list.html"
    context_object_name = "products"

class ProductCreateView(CreateView):
    model = Product
    template_name = "product_create.html"
    context_object_name = "products"
    fields = "__all__"
    success_url = reverse_lazy("product_list")

class ProductDetailView(DetailView):
    model = Product
    template_name = "product_detail.html"
    context_object_name = "product"

class ProductUpdateView(UpdateView):
    model = Product
    template_name = "product_update.html"
    context_object_name = "product"
    fields = "__all__"
    success_url = reverse_lazy("product_list")

    def post(self, request, pk):
        product = Product.objects.filter(pk=pk)
        items = request.POST.dict()
        del items['csrfmiddlewaretoken']
        product.update(**items)
        return HttpResponseRedirect(reverse('product_detail', args=(pk,)))

class ProductDeleteView(DeleteView):
    model = Product
    template_name = 'product_delete.html'
    context_object_name = 'product'
    success_url = reverse_lazy('product_list')

    def post(self, request, pk, *args, **kwargs):
        # print(request.POST)
        # return render(request, "product_list.html", {})
        if "cancel" in request.POST:
            return render(request, "product_list.html", {})
        else:
            super().post(request, *args, **kwargs)

class CategoryListView(ListView):
    model = Category
    template_name = 'category_list.html'
    context_object_name = 'categories'

